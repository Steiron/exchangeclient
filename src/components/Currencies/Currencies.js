import React from "react";
import {NavLink} from "react-router-dom";
import "./Currencies.scss";

export const Currencies = ({currencies}) => {

    const items = currencies.map((currency, idx) => {
        const {id, name, count, datePrices} = currency;
        let {date, rate} = datePrices[0];
        date = new Date(date).toLocaleDateString("ru-RU");
        return (
            <div className={"currency-item"} key={id}>
                <div className={"item-cell index"}>{idx}</div>
                <div className="item-cel name"><NavLink to={`/currency/${name.toLowerCase()}`}
                                                        className={"link"}> {name}</NavLink></div>
                <div className={"count item-cell"}>{count}</div>
                <div className={"date item-cell"}>{date}</div>
                <div className={"rate item-cell"}>{rate}</div>
            </div>
        );
    });


    return (
        <div className={"page-content currencies"}>
            <h1 className={"page-header"}>Обменый курс валют кроны</h1>
            <div className={"currencies-table"}>
                <div className={"currency-header"}>
                    <div className={"index header-item"}>#</div>
                    <div className={"name header-item"}>Валюта</div>
                    <div className={"count header-item"}>Количество</div>
                    <div className={"date header-item"}>Дата</div>
                    <div className={"rate header-item"}>Курс</div>
                </div>
                <div className={"currency-body"}>{items}</div>
            </div>
        </div>
    );
};