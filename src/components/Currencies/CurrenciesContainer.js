import React, {useEffect} from "react";
import {connect} from "react-redux";
import {compose} from "redux";
import {getAllCurrencies} from "../../redux/reducers/exchange-reducer";
import {Currencies} from "./Currencies";


const CurrenciesContainer = (props) => {

    const {getAllCurrencies} = props;

    useEffect(() => {
        getAllCurrencies();
    }, []);

    return <Currencies currencies={props.currencies}/>;
};

const mapStateToProps = (state) => {
    return ({
        currencies: state.exchange.currencies,
    });
};


export default compose(connect(mapStateToProps, {getAllCurrencies}))(CurrenciesContainer);