import React, {useEffect} from "react";
import {connect} from "react-redux";
import {withRouter} from "react-router";
import {compose} from "redux";
import {changeDay, initialCurrency, updateCurrency} from "../../../redux/reducers/exchange-reducer";
import {CurrencyInfo} from "./CurrencyInfo";
import "./CurrencyInfo.scss";

const CurrencyInfoContainer = (props) => {

        const {updateCurrency, initialCurrency, changeDay} = props;
        const {selectedCurrencyId, currentDate, currencies, filteredDate} = props;


        useEffect(() => {
            const currencyCode = props.match.params.currencyCode;
            initialCurrency(currencyCode);

        }, []);


        const onChangeDate = (e) => {
            const target = e.target;
            const newDate = target.value;

            const oldDate = new Date(currentDate);
            const updateDate = new Date(newDate);
            if (oldDate.getMonth() !== updateDate.getMonth() || oldDate.getFullYear() !== updateDate.getFullYear()) {
                updateCurrency(updateDate, selectedCurrencyId, oldDate);
            } else if (oldDate.getDay() !== updateDate.getDay()) {
                changeDay(updateDate);
            }
        };

        const onChangeCurrency = (e) => {
            const target = e.target;
            const newCurrencyId = target.value;

            if (selectedCurrencyId !== newCurrencyId) {
                updateCurrency(currentDate, newCurrencyId, currentDate);
            }
        };

        return (
            filteredDate !== null ?
                <CurrencyInfo currencies={currencies}
                              currency={filteredDate}
                              currentDate={currentDate}
                              selectedCurrencyId={selectedCurrencyId}
                              onChangeCurrency={onChangeCurrency}
                              onChangeDate={onChangeDate}
                /> : null
        );
    }
;

const mapStateToProps = (state) => {
    return ({
        currencies: state.exchange.currencies,
        filteredDate: state.exchange.filteredDate,
        selectedCurrencyId: state.exchange.selectedCurrencyId,
        currentDate: state.exchange.currentDate
    });
};


export default compose(
    connect(mapStateToProps,
        {
            updateCurrency,
            changeDay,
            initialCurrency,
        }
    ),
    withRouter
)(CurrencyInfoContainer);