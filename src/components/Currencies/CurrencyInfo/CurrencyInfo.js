import moment from "moment";
import React from "react";

export const CurrencyInfo = ({currencies, currency, currentDate, selectedCurrencyId, onChangeCurrency, onChangeDate}) => {

    const {name, count, datePrices} = currency;
    const {rate} = datePrices;
    const date = new Date(currentDate).toLocaleDateString("ru-RU");
    const dateFormat = moment(currentDate).format("YYYY-MM-DD");

    const currencySelect = (
        <select className={"input currency-select"} onChange={(e) => onChangeCurrency(e)}>
            {
                currencies.map((cur) => {
                    const {name, id} = cur;
                    const isSelected = id === selectedCurrencyId;
                    return (
                        <option value={id} selected={isSelected}>{name}</option>
                    );
                })
            }
        </select>
    );

    return (
        <div className={"page-content currency"}>
            <h1 className={"page-header"}>Курс валюты {name} на {date}</h1>
            <div className={"currency-block"}>

                <div className={"block-header"}>
                    <div className={"date-container"}>
                        <label className="container-header">Дата</label>
                        <input type="date" className={"input"} value={dateFormat} onChange={(e) => onChangeDate(e)}/>
                    </div>
                    <div className={"currency-container"}>
                        <label className="container-header">Валюта</label>
                        {currencySelect}
                    </div>
                </div>
                <div className={"block-body"}>
                    <div className={"count-container"}>{count} {name} =</div>
                    <div className={"rate-container"}>{rate} <span className="crown">CZK</span></div>
                </div>
            </div>
        </div>
    );
};