import React from 'react';
import {Route} from "react-router";
import {Switch} from "react-router-dom";
import CurrenciesContainer from "../Currencies/CurrenciesContainer";
import CurrencyInfoContainer from "../Currencies/CurrencyInfo/CurrencyInfoContainer";
import './App.scss';


const App = () => {
    return (
        <>
            {/*<Header />*/}
            <div className="content">
                <Switch>
                    <Route path={"/currency/:currencyCode"} render={() => <CurrencyInfoContainer/>}/>
                    <Route path={"/"} render={() => <CurrenciesContainer/>}/>
                </Switch>
            </div>
            {/*<Footer />*/}
        </>
    );
};

export default App;
