import {applyMiddleware, combineReducers, createStore} from "redux";
import thunk from "redux-thunk";
import {exchangeReducer} from "./reducers/exchange-reducer";

const reducers = combineReducers({
    exchange: exchangeReducer
});

const reduxStore = createStore(reducers, applyMiddleware(thunk));

export default reduxStore;