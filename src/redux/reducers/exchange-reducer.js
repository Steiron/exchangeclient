import {exchangeApi} from "../../api/exchange-api";
import {
    CHANGE_DATE,
    CHANGE_DAY,
    GET_CURRENCIES,
    INITIAL_CURRENCY,
    SET_CURRENCY
} from "../actions/exchange/exchange-action-type";

const initialState = {
    currencies: [],
    selectedCurrencyId: null,
    currency: null,
    currentDate: null,
    filteredDate: null
};


const filterDate = (date, currency) => {
    const filtered = currency.datePrices.filter(d => {
        const currencyDate = new Date(d.date);
        return currencyDate <= date;
    }).pop();
    return {...currency, datePrices: filtered};
};


export const exchangeReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_CURRENCIES:
            return {...state, currencies: action.payload};
        case INITIAL_CURRENCY: {
            const {currencies, currency, currentDate} = action.payload;
            return {
                ...state,
                currencies: currencies,
                currency: currency,
                selectedCurrencyId: currency.id,
                currentDate: currentDate,
                filteredDate: filterDate(currentDate, currency)
            };
        }
        case SET_CURRENCY:
            const currency = action.payload;
            return {
                ...state,
                currency: currency,
                selectedCurrencyId: currency.id,
                filteredDate: filterDate(state.currentDate, currency)
            };
        case CHANGE_DATE:
            return {
                ...state,
                currentDate: action.payload
            };
        case CHANGE_DAY:
            return {
                ...state,
                currentDate: action.payload,
                filteredDate: filterDate(action.payload, state.currency)
            };
        default:
            return state;
    }
};

const _getCurrencies = (currencies) => ({type: GET_CURRENCIES, payload: currencies});
const _setCurrency = (currency) => ({type: SET_CURRENCY, payload: currency});
const _changeDate = (newDate) => ({type: CHANGE_DATE, payload: newDate});
const _initialCurrency = (currencies, currency, currentDate) => ({
    type: INITIAL_CURRENCY,
    payload: {currencies, currency, currentDate}
});

export const changeDay = (newDate) => ({type: CHANGE_DAY, payload: newDate});


export const getAllCurrencies = () => async (dispatch) => {
    const response = await exchangeApi.getAll();
    dispatch(_getCurrencies(response.data));
};


export const updateCurrency = (date, currencyId, currentDate = null) => async (dispatch) => {
    const month = date.getMonth() + 1;
    const year = date.getFullYear();
    const response = await exchangeApi.getByIdWithDate(currencyId, month, year);
    if (currentDate !== date) {
        dispatch(_changeDate(date));
    }
    dispatch(_setCurrency(response.data));
};

export const initialCurrency = (name) => async (dispatch, getState) => {
    let allCurrency = getState().exchange.currencies;

    if (!allCurrency.length) {
        const allCurrencyResponse = await exchangeApi.getAll();
        allCurrency = allCurrencyResponse.data;
    }
    const tempCurrency = allCurrency.filter(c => c.name.toLowerCase() === name);
    const currencyId = tempCurrency.pop().id;
    const currentDate = new Date();
    const currencyResponse = await exchangeApi.getByIdWithDate(currencyId, currentDate.getMonth() + 1, currentDate.getFullYear());
    dispatch(_initialCurrency(allCurrency, currencyResponse.data, currentDate));
};