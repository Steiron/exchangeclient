import * as axios from "axios";


const controllerName = "exchange";


const instance = axios.create({
    baseURL: "http://localhost:5000/api/"
});


export const exchangeApi = {
    getAll() {
        return instance.get(controllerName);
    },
    getByIdWithDate(id, month, year) {
        return instance.post(`${controllerName}/${id}`, {month, year});
    }
};